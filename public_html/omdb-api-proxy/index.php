<?php
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Expose-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Origin");
    header('Content-type: application/json');

    $ini_file_path = "../../outOfDocumentroot/omdb.ini";
    $do_request = false;

    $ini_array = parse_ini_file($ini_file_path);

    require($ini_array["autoloader_path"]);
    use Symfony\Component\HttpClient\HttpClient;

    $prim_key = $ini_array["key1"];

    if(array_key_exists('realkey', $_GET)){
      // realkey should contain a application key of omdb of user 
      $prim_key = $_GET["realkey"];
    }

    if(array_key_exists('key', $_GET) && $_GET['key'] == "^gGfe4-HsJk7-Kr9O"){
      // key is  application key for this api proxy
      $do_request = true;
    }else{
      echo '{"error": "authorization failure"}';
    }

    if($do_request){
      $request_url = "https://www.omdbapi.com/?apikey=".$prim_key;
      $get_array = $_GET;
      foreach($get_array  as $key => $value){
      // futher enlarge the request url
        if($key !== "key"){
          $request_url .= "&".$key."=".$value;
        }
     }
     $httpClient = HttpClient::create();

     $response = $httpClient->request('GET', $request_url);

     $statusCode = $response->getStatusCode();

     if($statusCode == "200"){
       $content = $response->getContent();
       echo $content; //. "\n";
     }else{
       echo '{"error": "unkown error"}';
     }
   }
